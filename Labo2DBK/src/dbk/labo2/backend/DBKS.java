package dbk.labo2.backend;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import dbk.labo2.exceptions.BooqueenSalbuespena;
import dbk.labo2.frontend.UI;

public final class DBKS {
	private static DBKS gureDBKS;

	public static DBKS getDBKS() {
		return gureDBKS != null ? gureDBKS : (gureDBKS = new DBKS());
	}

	private Connection konexioa;

	private DBKS() {
	}

	public void aginduaExekutatu(String agindua) {
		try {
			Statement st = this.konexioa.createStatement();
			st.execute(agindua);
		} catch (Exception salbuespena) {
			UI.getNireUI().hutsaIpini();
			throw new BooqueenSalbuespena("Ezin da " + agindua + " exekutatu", salbuespena);
		}
	}

	public void deskonektatu() {
		try {
			this.konexioa.close();
		} catch (SQLException e) {
			throw new BooqueenSalbuespena("Ezin da deskonexioa egin", e);
		}
		try {
			if (konexioa.isClosed()) {
				JOptionPane.showMessageDialog(UI.getNireUI(), "Datu basetik deskonektatu zara", "Booqueen",
						JOptionPane.WARNING_MESSAGE);
				UI.getNireUI().setTitle("Booqueen");
			}
		} catch (HeadlessException | SQLException e) {
			e.printStackTrace();
		}
	}

	public void eguneraketaExekutatu(String agindua) {
		try {
			Statement st = this.konexioa.createStatement();
			st.executeUpdate(agindua);
		} catch (Exception salbuespena) {
			UI.getNireUI().hutsaIpini();
			throw new BooqueenSalbuespena("Ezin da " + agindua + " exekutatu", salbuespena);
		}
	}

	public void konektatu(String erabiltzailea, String pasahitza, String uri, String ataka) {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException salbuespena) {
			salbuespena.printStackTrace();
		}
		try {
			this.konexioa = DriverManager.getConnection(
					"jdbc:mysql://" + uri + ":" + ataka + "?user=" + erabiltzailea + "&password=" + pasahitza);
			this.konexioa.setAutoCommit(true);
		} catch (SQLException salbuespena) {
			throw new BooqueenSalbuespena("Ezin da datu basera konektatu", salbuespena);
		}
		JOptionPane.showMessageDialog(UI.getNireUI(), erabiltzailea + " erabiltzailearekin kautotu zara", "Booqueen",
				JOptionPane.INFORMATION_MESSAGE);
	}

	public ResultSet queryExekutatu(String agindua) {
		ResultSet emaitza = null;
		try {
			Statement st = this.konexioa.createStatement();
			emaitza = st.executeQuery(agindua);
		} catch (Exception salbuespena) {
			UI.getNireUI().hutsaIpini();
			throw new BooqueenSalbuespena("Ezin da " + agindua + " exekutatu", salbuespena);
		}
		return emaitza;
	}
}
