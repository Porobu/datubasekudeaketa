package dbk.labo2.backend;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Operazioak {
	public ArrayList<String[]> datuakJaso(String mota) {
		int kop = 0;
		String agindua = "";
		if (mota.equals("offer")) {
			kop = 5;
			agindua = "SELECT * FROM booqueen.Offer";
		} else if (mota.equals("booking")) {
			kop = 7;
			agindua = "SELECT * from booqueen.Booking";
		} else if (mota.equals("customer")) {
			kop = 4;
			agindua = "SELECT * FROM booqueen.Customer";
		} else
			return new ArrayList<>();
		ResultSet rs = DBKS.getDBKS().queryExekutatu(agindua);
		return this.rsKopiatu(rs, kop);
	}

	public void eguneratu(String zein, String[] sartzeko) {
		String agindua = "";
		if (zein.equalsIgnoreCase("insert")) {
			agindua = "INSERT INTO booqueen.Offer values(" + sartzeko[0] + "," + sartzeko[1] + ",'" + sartzeko[2]
					+ "','" + sartzeko[3] + "'," + sartzeko[4] + ")";
			DBKS.getDBKS().aginduaExekutatu(agindua);
		} else {
			agindua = "UPDATE booqueen.Offer SET oChain=" + sartzeko[1] + ",oStartingAt='" + sartzeko[2]
					+ "',oFinishingAt='" + sartzeko[3] + "',oPrice=" + sartzeko[4] + " where oCode=" + sartzeko[0];
			DBKS.getDBKS().eguneraketaExekutatu(agindua);
		}
	}

	private ArrayList<String[]> rsKopiatu(ResultSet rs, int kop) {
		ArrayList<String[]> emaitza = new ArrayList<>();
		try {
			while (rs.next()) {
				String[] oraingoa = new String[kop];
				for (int i = 0; i < kop; i++) {
					oraingoa[i] = rs.getString(i + 1);
				}
				emaitza.add(oraingoa);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emaitza;
	}

}
