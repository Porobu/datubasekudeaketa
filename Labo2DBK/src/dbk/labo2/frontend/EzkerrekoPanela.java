package dbk.labo2.frontend;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import dbk.labo2.backend.Operazioak;
import dbk.labo2.externals.SpringUtilities;

public class EzkerrekoPanela extends JPanel {

	private static final long serialVersionUID = 2551873781679121830L;
	private JLabel codeL, chainL, startingAtL, finishingAtL, priceL;
	private JButton insert, update, offer, booking, customer, itxi;
	private JTextField code, chain, startingAt, finishingAt, price;

	public EzkerrekoPanela() {
		code = new JTextField();
		codeL = new JLabel("Code:", SwingConstants.TRAILING);
		chainL = new JLabel("Chain:", SwingConstants.TRAILING);
		chain = new JTextField();
		startingAt = new JTextField();
		startingAtL = new JLabel("Starting At:", SwingConstants.TRAILING);
		finishingAt = new JTextField();
		finishingAtL = new JLabel("Finishing At:", SwingConstants.TRAILING);
		price = new JTextField();
		priceL = new JLabel("Price:", SwingConstants.TRAILING);
		codeL.setLabelFor(code);
		chainL.setLabelFor(chain);
		startingAtL.setLabelFor(startingAt);
		finishingAtL.setLabelFor(finishingAt);
		priceL.setLabelFor(price);
		insert = new JButton("Insert");
		update = new JButton("Update");
		insert.addActionListener(nireAE -> eguneratu("insert"));
		update.addActionListener(nireAE -> eguneratu("update"));
		offer = new JButton("Consult Offer");
		booking = new JButton("Consult Booking");
		customer = new JButton("Consult Customer");
		offer.addActionListener(nireAE -> UI.getNireUI().mezuakAldatu("offer"));
		booking.addActionListener(nireAE -> UI.getNireUI().mezuakAldatu("booking"));
		customer.addActionListener(nireAE -> UI.getNireUI().mezuakAldatu("customer"));
		itxi = new JButton("Itxi");
		itxi.addActionListener(nireAE -> System.exit(0));
		this.setLayout(new SpringLayout());
		this.add(codeL);
		this.add(code);
		this.add(chainL);
		this.add(chain);
		this.add(startingAtL);
		this.add(startingAt);
		this.add(finishingAtL);
		this.add(finishingAt);
		this.add(priceL);
		this.add(price);
		this.add(insert);
		this.add(update);
		this.add(offer);
		this.add(booking);
		this.add(customer);
		this.add(itxi);
		SpringUtilities.makeCompactGrid(this, 8, 2, 1, 1, 3, 3);
	}

	private void eguneratu(String zein) {
		Operazioak nireOperazioak = new Operazioak();
		nireOperazioak.eguneratu(zein, new String[] { code.getText(), chain.getText(), startingAt.getText(),
				finishingAt.getText(), price.getText() });
	}

}
