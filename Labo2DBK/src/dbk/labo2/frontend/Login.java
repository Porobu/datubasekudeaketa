package dbk.labo2.frontend;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import dbk.labo2.backend.DBKS;
import dbk.labo2.externals.SpringUtilities;

public class Login extends JPanel {
	private static final long serialVersionUID = -7323271088278163192L;
	private JLabel erabiltzailea, pasahitza, uriL, atakaL;
	private JTextField erabiltzaileaField, uri, ataka;
	private JPasswordField pasahitzaField;
	private JButton sartu, deskonektatu;

	private String erabiltzaileaString, pasahitzaString;

	public Login() {
		this.setLayout(new SpringLayout());
		this.uriL = new JLabel("Zerbitzariaren alias / IP helbidea:", SwingConstants.TRAILING);
		this.uri = new JTextField();
		this.uri.setText("datubasekudeaketa.cloudapp.net");
		this.uriL.setLabelFor(uri);
		this.add(uriL);
		this.add(uri);
		this.atakaL = new JLabel("Ataka:", SwingConstants.TRAILING);
		this.ataka = new JTextField();
		this.ataka.setText("3306");
		this.atakaL.setLabelFor(ataka);
		this.add(atakaL);
		this.add(ataka);
		this.erabiltzailea = new JLabel("Erabiltzailea:", SwingConstants.TRAILING);
		this.add(erabiltzailea);
		this.erabiltzaileaField = new JTextField(15);
		this.erabiltzailea.setLabelFor(erabiltzaileaField);
		this.add(erabiltzaileaField);
		this.pasahitza = new JLabel("Pasahitza:", SwingConstants.TRAILING);
		this.add(pasahitza);
		this.pasahitzaField = new JPasswordField(15);
		this.pasahitza.setLabelFor(pasahitzaField);
		this.add(pasahitzaField);
		this.sartu = new JButton("Sartu");
		this.sartu.addActionListener(gureAE -> this.datuakGorde());
		this.add(this.sartu);
		this.deskonektatu = new JButton("Deskonektatu");
		this.deskonektatu.addActionListener(nireAE -> this.deskonektatu());
		this.add(deskonektatu);
		SpringUtilities.makeCompactGrid(this, 5, 2, 1, 1, 3, 3);
		this.deskonektatu.setEnabled(false);
		this.setVisible(true);
	}

	private void datuakGorde() {
		this.erabiltzaileaString = this.erabiltzaileaField.getText();
		this.pasahitzaString = new String(this.pasahitzaField.getPassword());
		DBKS.getDBKS().konektatu(erabiltzaileaString, pasahitzaString, uri.getText(), ataka.getText());
		this.sartu.setEnabled(false);
		UI.getNireUI().setTitle("Booqueen - " + erabiltzaileaString);
		this.deskonektatu.setEnabled(true);
	}

	private void deskonektatu() {
		DBKS.getDBKS().deskonektatu();
		this.deskonektatu.setEnabled(false);
		this.sartu.setEnabled(true);
		UI.getNireUI().setTitle("Booqueen");
	}

}
