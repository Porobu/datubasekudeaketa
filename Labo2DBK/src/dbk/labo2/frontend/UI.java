package dbk.labo2.frontend;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.TableRowSorter;

import dbk.labo2.externals.ZebraJTable;

public class UI extends JFrame {

	private static final long serialVersionUID = -6028301898792889425L;
	private static UI nireUI;

	public static UI getNireUI() {
		return nireUI;
	}

	private JTable nireTaula;
	private JScrollPane nireJScrollPane;
	private JPanel ezkerrekoa;
	private Login nireLogin;

	public UI() {
		this.setLayout(new BorderLayout());
		this.setTitle("Booqueen");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.mezuak("");
		ezkerrekoa = new JPanel();
		ezkerrekoa.setLayout(new BorderLayout());
		ezkerrekoa.add(new EzkerrekoPanela(), BorderLayout.PAGE_START);
		this.add(ezkerrekoa, BorderLayout.LINE_START);
		nireLogin = new Login();
		this.add(nireLogin, BorderLayout.PAGE_START);
		this.pack();
		this.setMinimumSize(new Dimension(400, 400));
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		nireUI = this;
	}

	public void hutsaIpini() {
		this.mezuak("");
		this.revalidate();
		this.repaint();
	}

	private void mezuak(String mota) {
		TaulaModeloa nireModeloa = new TaulaModeloa(mota);
		nireTaula = new ZebraJTable(nireModeloa);
		nireTaula.setRowSorter(new TableRowSorter<>(nireModeloa));
		nireTaula.setFont(new Font("Arial", Font.PLAIN, 14));
		nireTaula.setPreferredSize(new Dimension(200, 200));
		nireJScrollPane = new JScrollPane(nireTaula);
		nireJScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		nireJScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.add(nireJScrollPane, BorderLayout.CENTER);
	}

	public void mezuakAldatu(String mota) {
		this.remove(nireJScrollPane);
		this.mezuak(mota);
		this.revalidate();
		this.repaint();
	}

}
