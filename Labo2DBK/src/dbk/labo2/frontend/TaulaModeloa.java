package dbk.labo2.frontend;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import dbk.labo2.backend.Operazioak;

public class TaulaModeloa extends AbstractTableModel {
	private static final long serialVersionUID = 5416722887047816684L;
	private String[] izenak;
	private ArrayList<String[]> datuak;

	public TaulaModeloa(String mota) {
		mota = mota.toLowerCase();
		if (mota.equals("offer")) {
			izenak = new String[] { "Code", "Chain", "Starting At", "Finishing At", "Price" };
		} else if (mota.equals("booking")) {
			izenak = new String[] { "Code", "Customer", "Hotel", "Offer", "Starting At", "Nights", "Paid" };
		} else if (mota.equals("customer")) {
			izenak = new String[] { "ID", "Name", "Phone", "Adress" };
		} else {
			izenak = new String[] { "Hutsik" };
		}
		Operazioak nireOperazioak = new Operazioak();
		datuak = nireOperazioak.datuakJaso(mota);
	}

	@Override
	public Class<String> getColumnClass(int c) {
		return String.class;
	}

	@Override
	public int getColumnCount() {
		return izenak.length;
	}

	@Override
	public String getColumnName(int i) {
		return izenak[i];
	}

	@Override
	public int getRowCount() {
		return datuak.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		return datuak.get(row)[col];
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}
}