package dbk.labo2.exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.exception.ExceptionUtils;

import dbk.labo2.frontend.UI;

public class BooqueenSalbuespena extends RuntimeException {
	private static final long serialVersionUID = -9212558555853305997L;

	public BooqueenSalbuespena(String mezua, Exception errorea) {
		String izenburua = "BOOQUEEN ERROREA!!!!!";
		System.err.println(izenburua + "\n");
		errorea.printStackTrace();
		String fitxategia = System.getProperty("user.home") + "/BooqueenErrorea.txt";
		if (System.getProperty("os.name").toLowerCase().contains("win"))
			fitxategia = System.getProperty("user.home") + "\\BooqueenErrorea.txt";
		try {
			PrintWriter gurePW = new PrintWriter(new File(fitxategia));
			gurePW.println(izenburua);
			gurePW.println(mezua);
			gurePW.println("Stack Trace:");
			gurePW.write(ExceptionUtils.getStackTrace(errorea));
			gurePW.flush();
			gurePW.close();
		} catch (FileNotFoundException e) {
		}
		String stackLaburra = ExceptionUtils.getRootCauseStackTrace(errorea)[0] + "\n"
				+ ExceptionUtils.getRootCauseStackTrace(errorea)[1] + "\nthrown "
				+ ExceptionUtils.getRootCauseStackTrace(this)[1];
		JOptionPane.showMessageDialog(UI.getNireUI(), mezua + "\nStack Trace Laburra:\n" + stackLaburra
				+ "\nStack trace " + fitxategia + " fitxategian gorde da.", izenburua, JOptionPane.ERROR_MESSAGE);

	}
}