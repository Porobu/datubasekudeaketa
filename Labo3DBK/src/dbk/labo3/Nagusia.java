package dbk.labo3;

import java.util.Random;

public class Nagusia {

	public static void main(String[] args) {
		DBKS.getDBKS().konektatu("auditor", "ImTheAuditor", "datubasekudeaketa.cloudapp.net", "3306");
		DBKS.getDBKS().aginduaExekutatu("use auditing");
		Random r = new Random();
		for (int i = 0; i < 20000; i++) {
			if (i % 10 == 0)
				System.out.println("i=" + i);
			DBKS.getDBKS().aginduaExekutatu(
					"call newA(" + r.nextInt(1000000) + ", " + r.nextInt(1000000) + ", '" + randomString(r) + "')");
			DBKS.getDBKS().aginduaExekutatu(
					"call newB(" + r.nextInt(1000000) + ", " + r.nextInt(1000000) + ", '" + randomString(r) + "')");
		}
	}

	private static StringBuilder randomString(Random r) {
		// 65-90, A-Z
		StringBuilder random = new StringBuilder(8);
		while (random.length() < 8)
			random = random.append((char) ((char) r.nextInt(26) + 65));
		return random;
	}
}
