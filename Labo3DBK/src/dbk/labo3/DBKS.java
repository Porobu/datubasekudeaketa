package dbk.labo3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class DBKS {
	private static DBKS gureDBKS;

	public static DBKS getDBKS() {
		return gureDBKS != null ? gureDBKS : (gureDBKS = new DBKS());
	}

	private Connection konexioa;

	private DBKS() {
	}

	public void aginduaExekutatu(String agindua) {
		try {
			Statement st = this.konexioa.createStatement();
			st.execute(agindua);
		} catch (Exception salbuespena) {
			salbuespena.printStackTrace();
		}
	}

	public void deskonektatu() {
		try {
			this.konexioa.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void eguneraketaExekutatu(String agindua) {
		try {
			Statement st = this.konexioa.createStatement();
			st.executeUpdate(agindua);
		} catch (Exception salbuespena) {
			salbuespena.printStackTrace();
		}
	}

	public void konektatu(String erabiltzailea, String pasahitza, String uri, String ataka) {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException salbuespena) {
			salbuespena.printStackTrace();
		}
		try {
			this.konexioa = DriverManager.getConnection(
					"jdbc:mysql://" + uri + ":" + ataka + "?user=" + erabiltzailea + "&password=" + pasahitza);
			this.konexioa.setAutoCommit(true);
		} catch (SQLException salbuespena) {
			salbuespena.printStackTrace();
		}
	}

	public ResultSet kontsultaExekutatu(String agindua) {
		ResultSet emaitza = null;
		try {
			Statement st = this.konexioa.createStatement();
			emaitza = st.executeQuery(agindua);
		} catch (Exception salbuespena) {
			salbuespena.printStackTrace();
		}
		return emaitza;
	}

}
