package labo6;

import java.io.BufferedOutputStream;
import java.io.PrintStream;
import java.util.Random;

public class Nagusia {
	private static Random rd;
	private static final String LETRAK = "abcdefghijklmnopqrstuvwxyz";

	public static void main(String[] args) {
		System.setOut(new PrintStream(new BufferedOutputStream(System.out)));
		rd = new Random();
		DBKS dbks = new DBKS();
		dbks.konektatu("txandapasa", "txandapasa");
		int salary = 0;
		String izena;
		String abizena;
		String hire_date = "2015-12-31";
		String gender;
		String birth_date = "1990-12-31";
		String toDate = "2017-12-31";
		for (int num_empl = 724000; num_empl < 950000; num_empl++) {
			// emp_no = num_empl
			salary = getSalary();
			izena = getString();
			abizena = getString();
			gender = getGender(num_empl);
			String add_employee = "INSERT INTO employees.employees "
					+ "(emp_no, first_name, last_name, hire_date, gender, birth_date) " + "VALUES (" + num_empl + ", '"
					+ izena + "', '" + abizena + "', '" + hire_date + "', '" + gender + "', '" + birth_date + "')";
			String add_salary = "INSERT INTO employees.salaries " + "(emp_no, salary, from_date, to_date) " + "VALUES ("
					+ num_empl + ", " + salary + ", '" + hire_date + "', '" + toDate + "')";
			dbks.aginduaExekutatu(add_employee);
			dbks.aginduaExekutatu(add_salary);
			System.out.println(num_empl + " salary --> " + salary);
			if (num_empl % 1000 == 0)
				dbks.commitEgin();
		}

	}

	private static String getString() {
		char[] text = new char[6];
		for (int i = 0; i < 6; i++) {
			text[i] = LETRAK.charAt(rd.nextInt(LETRAK.length()));
		}
		return new String(text);
	}

	private static int getSalary() {
		return rd.nextInt(40000);
	}

	private static String getGender(int i) {
		if (i % 2 == 0)
			return ("M");
		else
			return ("F");
	}
}
