package labo6;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBKS {

	private static DBKS dbks;
	private Connection conn;
	private Statement st;

	public DBKS() {
	}

	public static DBKS getDBKS() {
		return dbks != null ? dbks : (dbks = new DBKS());
	}

	public void konektatu(String erab, String pass) {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			this.conn = DriverManager.getConnection("jdbc:mysql://datubasekudeaketa.cloudapp.net:3306", erab, pass);
			System.out.println("Ondo konektatu da.");
			conn.setAutoCommit(false);
			st = conn.createStatement();
		} catch (SQLException e) {
			System.out.println("Ezin izan da datu basera konektatu");
			e.printStackTrace();
		}
	}

	public void aginduaExekutatu(String agindua) {
		try {
			st.execute(agindua);
		} catch (Exception salbuespena) {
			System.err.println("Ezin da " + agindua + " exekutatu.");
		}
	}

	public void commitEgin() {
		try {
			conn.commit();
		} catch (SQLException e) {
			System.err.println("Ezin da commit exekutatu!");
		}
	}

}
