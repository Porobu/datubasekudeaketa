package dbk.labo5;

import java.lang.reflect.InvocationTargetException;

public class RunnableX implements Runnable {
	private int mode;
	private String izena;
	private DBKS dbks;

	public RunnableX(int mode, String izena) {
		this.dbks = new DBKS();
		this.mode = mode;
		this.izena = izena;
	}

	@Override
	public void run() {
		int kontagailua = 0;
		boolean commited = false;
		while (kontagailua < Labo5Constants.ITERAZIO_KOPURUA) {
			try {
				commited = dbks.transactionX(mode, izena, kontagailua, dbks);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				e.printStackTrace();
				commited = false;
			}
			if (commited)
				kontagailua++;
		}
	}
}
