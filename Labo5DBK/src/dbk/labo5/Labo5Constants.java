package dbk.labo5;

public interface Labo5Constants {
	public static final int ITERAZIO_KOPURUA = 100;
	public static final int LOCKING = 1;
	public static final int NONLOCKING = 0;
	public static final String TRANSACTION_A = "transactionA";
	public static final String TRANSACTION_B = "transactionB";
	public static final String TRANSACTION_C = "transactionC";
	public static final String ZERBITZARIA = "datubasekudeaketa.cloudapp.net";
	public static final String LOCALHOST = "localhost";
}
