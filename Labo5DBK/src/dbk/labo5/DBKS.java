package dbk.labo5;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBKS implements Labo5Constants {

	private Connection konexioa;
	private Statement st;

	public DBKS() {
		this.konektatu("Threads", "Labo5DBK", Labo5Constants.ZERBITZARIA, "3306");
	}

	private void eguneraketaExekutatu(String agindua) throws SQLException {
		this.st.executeUpdate(agindua);
	}

	private void eguneraketaX(int mode, String name, int kontagailua) throws SQLException {
		int xValue = this.getXValue(mode);
		this.setXValue(xValue + 1);
		System.out.println("WRITE (" + name + "," + kontagailua + ",X," + xValue + "," + (xValue + 1) + ")");
	}

	private void eguneraketaY(int mode, String name, int kontagailua) throws SQLException {
		int yValue = this.getYValue(mode);
		this.setYValue(yValue + 1);
		System.out.println("WRITE (" + name + "," + kontagailua + ",Y," + yValue + "," + (yValue + 1) + ")");
	}

	private void eguneraketaZ(int mode, String name, int kontagailua) throws SQLException {
		int zValue = this.getZValue(mode);
		this.setZValue(zValue + 1);
		System.out.println("WRITE (" + name + "," + kontagailua + ",Z," + zValue + "," + (zValue + 1) + ")");
	}

	private int getXValue(int mode) throws SQLException {
		String agindua = "Select value from concurrency_control.variables where name = 'X'"
				+ (mode == Labo5Constants.LOCKING ? "FOR UPDATE" : "");
		ResultSet rs = this.kontsultaExekutatu(agindua);
		rs.next();
		return rs.getInt(1);

	}

	private int getYValue(int mode) throws SQLException {
		String agindua = "Select value from concurrency_control.variables where name = 'Y' "
				+ (mode == Labo5Constants.LOCKING ? "FOR UPDATE" : "");
		ResultSet rs = this.kontsultaExekutatu(agindua);

		rs.next();
		return rs.getInt(1);

	}

	private int getZValue(int mode) throws SQLException {
		String agindua = "Select value from concurrency_control.variables where name = 'Z' "
				+ (mode == Labo5Constants.LOCKING ? "FOR UPDATE" : "");
		ResultSet rs = this.kontsultaExekutatu(agindua);

		rs.next();
		return rs.getInt(1);

	}

	public void initilizeSharedVariables() throws SQLException {
		String agindua = "UPDATE concurrency_control.variables SET value = 0";
		this.eguneraketaExekutatu(agindua);
		this.konexioa.commit();
	}

	private void konektatu(String erabiltzailea, String pasahitza, String uri, String ataka) {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException salbuespena) {
			salbuespena.printStackTrace();
		}
		try {
			this.konexioa = DriverManager.getConnection(
					"jdbc:mysql://" + uri + ":" + ataka + "?user=" + erabiltzailea + "&password=" + pasahitza);
			this.konexioa.setAutoCommit(false);
			this.st = konexioa.createStatement();
		} catch (SQLException salbuespena) {
			salbuespena.printStackTrace();
		}
	}

	private ResultSet kontsultaExekutatu(String agindua) {
		ResultSet emaitza = null;
		try {
			emaitza = this.st.executeQuery(agindua);
		} catch (Exception salbuespena) {
			salbuespena.printStackTrace();
		}
		return emaitza;
	}

	private void setXValue(int value) throws SQLException {
		String agindua = "UPDATE concurrency_control.variables SET value = " + value + " where name = 'X'";
		this.eguneraketaExekutatu(agindua);
	}

	private void setYValue(int value) throws SQLException {
		String agindua = "UPDATE concurrency_control.variables SET value = " + value + " where name = 'Y'";
		this.eguneraketaExekutatu(agindua);
	}

	private void setZValue(int value) throws SQLException {
		String agindua = "UPDATE concurrency_control.variables SET value = " + value + " where name = 'Z'";
		this.eguneraketaExekutatu(agindua);
	}

	public void showValues() throws SQLException {
		String agindua = "Select * from concurrency_control.variables";
		ResultSet rs = this.kontsultaExekutatu(agindua);
		try {
			while (rs.next())
				System.out.println(rs.getString(1) + ": " + rs.getString(2));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.konexioa.commit();
	}

	@SuppressWarnings("unused")
	private boolean transactionA(int mode, String name, int kontagailua) {
		try {
			this.eguneraketaX(mode, name, kontagailua);
			this.eguneraketaY(mode, name, kontagailua);
			this.konexioa.commit();
			return true;
		} catch (SQLException e) {
			System.err.println("Rollback transaction A");
			try {
				this.konexioa.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return false;
		}
	}

	@SuppressWarnings("unused")
	private boolean transactionB(int mode, String name, int kontagailua) throws SQLException {
		try {

			this.eguneraketaY(mode, name, kontagailua);
			this.eguneraketaZ(mode, name, kontagailua);
			this.konexioa.commit();
			return true;
		} catch (SQLException e) {
			System.err.println("Rollback transaction B");
			try {
				this.konexioa.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return false;
		}
	}

	@SuppressWarnings("unused")
	private boolean transactionC(int mode, String name, int kontagailua) throws SQLException {
		try {
			this.eguneraketaZ(mode, name, kontagailua);
			this.eguneraketaX(mode, name, kontagailua);
			this.konexioa.commit();
			return true;
		} catch (SQLException e) {
			System.err.println("Rollback transaction C");
			try {
				this.konexioa.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return false;
		}
	}

	public Boolean transactionX(int mode, String name, int kontagailua, DBKS oraingoa) throws NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method transaction = DBKS.class.getDeclaredMethod(name, int.class, String.class, int.class);
		return (Boolean) transaction.invoke(oraingoa, mode, name, kontagailua);
	}
}
