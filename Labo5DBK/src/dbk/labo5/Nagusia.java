package dbk.labo5;

import java.sql.SQLException;

public class Nagusia {
	public static void main(String[] args) throws SQLException {
		if (args.length < 1) {
			System.err.println("Programak funtzionateko argumentuetan modua behar zaio, 'LOCKING' edo 'NONLOCKING'");
			System.exit(1);
		}
		Nagusia n = new Nagusia(
				args[0].equalsIgnoreCase("LOCKING") ? Labo5Constants.LOCKING : Labo5Constants.NONLOCKING);
		n.hasieratu();
	}
	private RunnableX a, b, c;
	private Thread ta, tb, tc;
	private DBKS dbks;

	private int mode;

	public Nagusia(int mode) {
		this.mode = mode;
		a = new RunnableX(mode, Labo5Constants.TRANSACTION_A);
		b = new RunnableX(mode, Labo5Constants.TRANSACTION_B);
		c = new RunnableX(mode, Labo5Constants.TRANSACTION_C);
		ta = new Thread(a);
		tb = new Thread(b);
		tc = new Thread(c);
		dbks = new DBKS();
	}

	private void hasieratu() throws SQLException {
		System.out.println("Aukeratutako modoa: " + (mode == Labo5Constants.LOCKING ? "LOCKING" : "NONLOCKING"));
		System.out.println("Aurreko balioak:");
		dbks.showValues();
		dbks.initilizeSharedVariables();
		System.out.println("Balioak hauek 0ra ipini eta gero:");
		dbks.showValues();
		ta.start();
		tb.start();
		tc.start();
		for (;;)
			if (!ta.isAlive() && !tb.isAlive() && !tc.isAlive()) {
				dbks.showValues();
				break;
			}
	}

}
